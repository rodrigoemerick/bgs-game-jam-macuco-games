﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PopupController : MonoBehaviour
{
    [SerializeField] private Strings m_SSO;

    public void GoToScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}