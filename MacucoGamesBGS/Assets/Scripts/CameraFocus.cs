﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocus : MonoBehaviour
{
    [SerializeField] private Transform m_BodyBall;
    [SerializeField] private Transform m_MindBall;
    [SerializeField] private BallStateManager m_BSMSO;
    [SerializeField] private float m_SmoothCamera;

    void Update()
    {
        if (m_BSMSO.m_IsMind)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(m_MindBall.transform.position.x, m_MindBall.transform.position.y, transform.position.z), m_SmoothCamera * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(m_BodyBall.transform.position.x, m_BodyBall.transform.position.y, transform.position.z), m_SmoothCamera * Time.deltaTime);
        }
    }
}