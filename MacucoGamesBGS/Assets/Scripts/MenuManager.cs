﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private Levels m_LSO;
    [SerializeField] private Animator m_Animator;
    [SerializeField] GameObject m_Ball, m_CameraBall, m_Ball2;
    [SerializeField] private Transform m_UpPosition, m_DownPosition, m_BottomPosition;
    [SerializeField] private float m_Smooth;

    private bool m_CanMove;

    public void GoToLevels()
    {
        m_LSO.maxLevel = PlayerPrefs.GetInt("MaxLevel", 0);
        m_LSO.selectedLevel = m_LSO.maxLevel;
        m_CanMove = true;
        GoTo("GoToLevels");
    }

    void Update()
    {
        if (m_CanMove)
        {
            m_Ball.transform.position = Vector3.Lerp(m_Ball.transform.position, m_UpPosition.position, m_Smooth * Time.deltaTime);
            m_CameraBall.transform.position = Vector3.Lerp(m_CameraBall.transform.position, m_UpPosition.position, m_Smooth * Time.deltaTime);
            m_Ball2.transform.position = Vector3.Lerp(m_Ball2.transform.position, m_DownPosition.position, m_Smooth * Time.deltaTime);
        }
        else
        {
            m_Ball.transform.position = Vector3.Lerp(m_Ball.transform.position, m_DownPosition.position, m_Smooth * Time.deltaTime);
            m_CameraBall.transform.position = Vector3.Lerp(m_CameraBall.transform.position, m_DownPosition.position, m_Smooth * Time.deltaTime);
            m_Ball2.transform.position = Vector3.Lerp(m_Ball2.transform.position, m_BottomPosition.position, m_Smooth * Time.deltaTime);
        }
    }

    public void GoTo(string trigger)
    {
        m_Animator.SetTrigger(trigger);
    }

    public void SetMoveFalse()
    {
        m_CanMove = false;
    }
}