﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointToGoal : MonoBehaviour
{
    GameObject m_Goal;

    void Start()
    {
        m_Goal = GameObject.FindGameObjectWithTag("Victory");
    }

    void Update()
    {
        transform.LookAt(m_Goal.transform.position, Vector3.right);
    }
}