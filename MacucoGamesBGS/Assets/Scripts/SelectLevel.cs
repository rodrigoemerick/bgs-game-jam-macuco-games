﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectLevel : MonoBehaviour
{
    [SerializeField] private LevelButton m_LevelButton;
    [SerializeField] private Levels m_LSO;
    
    public void ChangeLevel(bool increase)
    {
        if (increase)
        {
            if (m_LSO.selectedLevel < m_LSO.maxLevel)
            {
                ++m_LSO.selectedLevel;
            }
            else
            {
                m_LSO.selectedLevel = 0;
            }
        }
        else
        {
            if (m_LSO.selectedLevel <= 0)
            {
                m_LSO.selectedLevel = m_LSO.maxLevel;
            }
            else
            {
                --m_LSO.selectedLevel;
            }
        }
    } 
}