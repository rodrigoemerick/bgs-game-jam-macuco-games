﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionLine : MonoBehaviour
{
    [SerializeField] private Transform m_BodyBall;
    [SerializeField] private Transform m_MindBall;
    [SerializeField] private LineRenderer m_Line;

    void Update()
    {
        Vector3 midline = new Vector3((m_MindBall.localPosition.x + m_BodyBall.localPosition.x)/2,
                                      (m_MindBall.localPosition.y + m_BodyBall.localPosition.y) / 2,
                                      m_MindBall.localPosition.z);
        m_Line.SetPosition(0, m_MindBall.localPosition);
        m_Line.SetPosition(1, midline);
        m_Line.SetPosition(2, m_BodyBall.localPosition);
    }
}