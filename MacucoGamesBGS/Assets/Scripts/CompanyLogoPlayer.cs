﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CompanyLogoPlayer : MonoBehaviour
{
    const float m_BreakAudioTime = 0.1f;
    const float m_MacucoAudioTime = 1.2f;
    const float m_ChangeSceneTime = 1.0f;

    [SerializeField] AudioSource m_AudioSource;
    [SerializeField] AudioClip m_BreakEgg, m_MacucoAudio;

    void Start()
    {
        StartCoroutine(PlayBreakEgg());
        StartCoroutine(PlayMacucoAudio());
    }

    IEnumerator PlayBreakEgg()
    {
        yield return new WaitForSeconds(m_BreakAudioTime);
        m_AudioSource.clip = m_BreakEgg;
        m_AudioSource.Play();
    }

    IEnumerator PlayMacucoAudio()
    {
        yield return new WaitForSeconds(m_MacucoAudioTime);
        m_AudioSource.clip = m_MacucoAudio;
        m_AudioSource.Play();
        StartCoroutine(GoToMenu());
    }

    IEnumerator GoToMenu()
    {
        yield return new WaitForSeconds(m_ChangeSceneTime);
        SceneManager.LoadScene("Menu");
    }
}