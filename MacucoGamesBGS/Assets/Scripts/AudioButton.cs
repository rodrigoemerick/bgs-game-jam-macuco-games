﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioButton : MonoBehaviour
{
    [SerializeField] private AudioMixer m_Master;
    [SerializeField] private Animator m_Animator;
    private bool m_Sounded;

    void Start()
    {
        m_Sounded = PlayerPrefs.GetInt("IsSounded", 1) == 1 ? true : false;
        m_Animator.SetBool("Sounded", m_Sounded);
        SetAudio();
    }

    void SetMaster(string group, float volume)
    {
        m_Master.SetFloat(group, volume);
    }

    void SetAudio()
    {
        if (!m_Sounded)
        {
            PlayerPrefs.SetInt("IsSounded", 0);
            SetMaster("Volume", -80.0f);
        }
        else
        {
            PlayerPrefs.SetInt("IsSounded", 1);
            SetMaster("Volume", 0.0f);
        }
    }

    public void ChangeAudio()
    {
        m_Sounded = !m_Sounded;
        m_Animator.SetBool("Sounded", m_Sounded);
        SetAudio();
    }
}