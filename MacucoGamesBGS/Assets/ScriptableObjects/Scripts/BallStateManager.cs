﻿using UnityEngine;

[CreateAssetMenu]
public class BallStateManager : ScriptableObject
{
    public bool m_IsMind;
    public delegate void SwitchCameraEvent();
    public SwitchCameraEvent SCEvent = null;
}